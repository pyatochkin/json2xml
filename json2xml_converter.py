#!/usr/local/bin/python3
from json2xml.utils import readfromjson
from dicttoxml import dicttoxml
from xml.dom.minidom import parseString
import argparse

def convert_json_to_xml(json_file_name, output_file_name):
    json = readfromjson(json_file_name)
    xml_data = dicttoxml(json, attr_type=False)
    xml_data = parseString(xml_data).toprettyxml()
    print(xml_data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='''
        Script converts json data format into xml.
        Input parameters: [1]json file name, [2]name of output converted file.
        ''', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('json_file_name', help='name of file in json format')
    parser.add_argument('out_file_name', help='name of output file')
    args = parser.parse_args()
    convert_json_to_xml(args.json_file_name, args.out_file_name)